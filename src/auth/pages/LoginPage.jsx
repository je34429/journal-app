import { Google } from "@mui/icons-material"
import { Button, Grid, Link, TextField, Typography } from "@mui/material"
import { useDispatch } from "react-redux"
import { Link as RouterLink } from "react-router-dom"
import { useForm } from "../../hooks"
import { checkingAuthentication, startGoogleSingIn } from "../../store/auth"
import { AuthLayout } from "../layout/AuthLayout"

export const LoginPage = () => {
  const {email, password, onInputChange, formState} = useForm({
    email:'jaime@gmail.com',
    password:'123456'
  });

  const dispatch = useDispatch();

  const onSubmit = (event)=>{
    event.preventDefault();
    console.log({email, password});
    dispatch(checkingAuthentication());
  }

  const onGoogleSignIn = ()=>{
    console.log('onGoogleSignIn');
    dispatch(startGoogleSingIn())
  }

  
  return (
      <AuthLayout title="Login">
        <form onSubmit={onSubmit}>
          <Grid container>
            <Grid item xs={12} sx={{ mt:2 }}>
              <TextField
                label='Correo'
                type='email'
                placeholder='correo@google.com'
                fullWidth
                name="email"
                onChange={onInputChange}
                value={email}
              />
            </Grid>
            <Grid item xs={12} sx={{ mt:2 }}>
              <TextField
                label='Contraseña'
                type='password'
                placeholder='Contraseña'
                fullWidth
                name="password"
                onChange={onInputChange}
                value={password}
              />
            </Grid>

            <Grid container spacing={2} sx={{ mt:1, mb:2 }}>
                <Grid item xs={12} sm={6}>
                  <Button variant='contained' fullWidth type="submit">
                    Login
                  </Button>
                </Grid>
                <Grid item xs={12} sm={6}>
                  <Button variant='contained' fullWidth onClick={onGoogleSignIn}>
                    <Google>
                      <Typography sx={{ml:1}}>Google</Typography>
                    </Google>
                  </Button>
                </Grid>
            </Grid>
          </Grid>
          <Grid container direction='row' justifyContent='end'>
            <Link color='inherit' to='/auth/register' component={RouterLink}>Crear una cuenta</Link>
          </Grid>
        </form>
      </AuthLayout>
  )
}
