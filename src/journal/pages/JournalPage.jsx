import { AddOutlined } from "@mui/icons-material"
import { IconButton } from "@mui/material"
import { JournalLayout } from "../layout/JournalLayout"
import { NothingSelectedView, NoteView } from "../views/"

export const JournalPage = () => {
  return (
    <JournalLayout>
      {/* <Typography>Deserunt pariatur eiusmod culpa pariatur Lorem nulla labore deserunt esse esse esse qui. Sunt eiusmod amet pariatur irure nulla. Fugiat magna laborum in deserunt eiusmod aute dolor. Duis ad ullamco Lorem amet occaecat esse ad veniam incididunt consequat nisi mollit irure. Ut mollit labore nulla occaecat enim laborum exercitation qui ut labore aliqua quis ea. Do excepteur deserunt eiusmod ullamco veniam esse duis amet excepteur mollit id qui laboris magna.</Typography> */}
      <NothingSelectedView/>
      {/* <NoteView/> */}
      <IconButton
        size='large'
        sx={{
          color:'white',
          backgroundColor:'error.main',
          ':hover':{opacity:0.9, backgroundColor:'error.main',},
          position:'fixed',
          right:50,
          bottom:50
        }}
      >
        <AddOutlined sx={{ fontSize:30 }}/>
      </IconButton>
    </JournalLayout>
  )
}
